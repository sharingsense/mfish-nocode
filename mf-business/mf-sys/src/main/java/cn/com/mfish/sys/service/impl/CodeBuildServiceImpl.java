package cn.com.mfish.sys.service.impl;

import cn.com.mfish.sys.entity.CodeBuild;
import cn.com.mfish.sys.mapper.CodeBuildMapper;
import cn.com.mfish.sys.service.CodeBuildService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @description: 代码构建
 * @author: mfish
 * @date: 2023-04-11
 * @version: V1.0.0
 */
@Service
public class CodeBuildServiceImpl extends ServiceImpl<CodeBuildMapper, CodeBuild> implements CodeBuildService {

}
